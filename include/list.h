#ifndef LIST_H
#define LIST_H

struct trie_t;

struct list_t {
        struct list_t *prev;
        struct list_t *next;

        struct trie_t *element;
        int len;
};

struct list_t *list_create();
void list_delete(struct list_t *head);
void list_insert(struct list_t *head, struct trie_t *root);

#endif // !LIST_H
