#ifndef LOADER_H
#define LOADER_H

#include "dump.h"

struct bin
{
        struct header_t *header;
        struct ftrie_t *trie;
        char *strtab;
        int file_len;
};

void load_bin(char *path, struct bin *bin);
void delete_bin(struct bin *bin);
#endif //LOADER_H
