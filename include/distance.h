#ifndef DISTANCE_H
#define DISTANCE_H

unsigned damerau_levenshtein(const char *w1, const char *w2,
                             unsigned t[MAX_WORD_LEN][MAX_WORD_LEN],
                             int s1, int s2);

#endif // !DISTANCE_H
