#ifndef PATRICIA_H
#define PATRICIA_H

#include "trie.h"
#include "strtab.h"

struct patricia_t {
        unsigned offset;
        struct patricia_t *child;
        struct patricia_t *bro;
        int frequency;
        unsigned id;
};

struct patricia_t *patricia_from_trie(struct trie_t *trie, int *n,
                                      struct strtab_t *strtab);
void patricia_print(struct patricia_t *root, struct strtab_t strtab);
void patricia_delete(struct patricia_t *root);

#endif // !PATRICIA_H
