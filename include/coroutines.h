#ifndef COROUTINES_H
#define COROUTINES_H

#include <ucontext.h>

struct coro_t {
    ucontext_t context;
    ucontext_t old_context;
    int ended;
    void *yield_value;
};

struct coro_t *coro_new();
void coro_free(struct coro_t *coro);
void *coro_resume(struct coro_t *coro);
void coro_yield(struct coro_t *coro, void *value);

#endif //COROUTINES_H
