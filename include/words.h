#ifndef WORDS_H
#define WORDS_H

struct words {
        char *word;
        unsigned freq;
        unsigned distance;
};

#endif //WORDS_H
