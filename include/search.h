#ifndef SEARCH_H
#define SEARCH_H

#include "loader.h"

struct search {
        struct words *words;
        unsigned len;
        unsigned capacity;
};

struct words *search_word(char *word, struct bin bin);
struct search *search_words(char *word, struct bin bin, unsigned approx);

#endif // !SEARCH_H
