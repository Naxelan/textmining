#ifndef FTRIETOOLS_H
#define FTRIETOOLS_H

#include "dump.h"
#include "loader.h"

struct ftrie_t *trie_bro(struct bin bin, struct ftrie_t *trie);
struct ftrie_t *trie_child(struct bin bin, struct ftrie_t *trie);
char *trie_str(struct bin bin, struct ftrie_t *trie);

#endif // !FTRIETOOLS_H
