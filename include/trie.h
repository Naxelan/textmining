#ifndef TRIE_H
#define TRIE_H

#define MAX_WORD_LEN 256

struct list_t;

struct trie_t {
        char key;
        struct list_t *children;
        int frequency;
};

struct trie_t *trie_create();
int trie_insert(struct trie_t *root, char *word, int frequency);
void trie_delete(struct trie_t *root);
void trie_print(struct trie_t *root);

#endif // !TRIE_H
