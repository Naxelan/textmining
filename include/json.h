#ifndef JSON_H
#define JSON_H

#include <stddef.h>
#include "words.h"

void json_printer(struct words *words, size_t len);

#endif //JSON_H
