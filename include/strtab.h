#ifndef STRTAB_H
#define STRTAB_H

struct strtab_t {
        char *str;
        unsigned len;
        unsigned capacity;
};

struct strtab_t strtab_create();
unsigned strtab_insert(struct strtab_t *strtab, char *str);
void strtab_delete(struct strtab_t strtab);
unsigned strtab_resize(struct strtab_t *strtab);

#endif // !STRTAB_H
