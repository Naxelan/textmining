#ifndef PARSER_C
#define PARSER_C

#include "trie.h"

struct parsed {
        char key[MAX_WORD_LEN];
        int freq;
};

int parser(FILE *file, struct parsed *parsed);

#endif //PARSER_C
