#ifndef DUMP_H
#define DUMP_H

#include "patricia.h"

struct header_t
{
        unsigned nb_node;
        unsigned nb_char;
};

struct ftrie_t
{
        unsigned id_strtab;
        unsigned id_child;
        unsigned id_bro;
        unsigned freq;
};

void dump(struct patricia_t *p, char *file, int n, struct strtab_t *strtab);

#endif
