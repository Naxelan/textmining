# TextMining

* Build System : CMake

Questions :
===========
 1.	Décrivez les choix de design de votre programme
 
Nous avons décidé de faire notre programme en C, langage nous permettant
d'avoir la main sur beaucoup de chose, et donc une plus grande liberté
d'optimisation.

 2.	Listez l’ensemble des tests effectués sur votre programme
    (en plus des units tests)

Pour tester notre programme, nous avions une CI, déclenchée à chaque
push, lançant le build, la compilation du dictionnaire, puis les tests
timés sur TextMiningApp. Celle ci testait plusieurs mots, avec des
distances différentes, puis des mots extremement longs.

 3.	Avez-vous détecté des cas où la correction par distance
    nefonctionnait pas (même avec une distance élevée) ?

Il faudrait être insensible à la casse.

 4.	Quelle est la structure de données que vous avez implémentée dans
    votre projet, pourquoi ?

Nous avons utilisé un patricia trie car c'était la structure de donnée
la plus compacte et la plus simple d'implémentation, de plus elle permet
un calcul de la distance de levenshtein lors du parcours de l'arbre.

 5.	Proposez un réglage automatique de la distance pour un programme qui
    prend juste une chaîne de caractères en entrée, donner le processus
    d’évaluation ainsi que les résultats

```
if(len(word) < 2):
        return 0
elif(len(word) < 4):
        return 1
return 2
```

 6.	Comment comptez vous améliorer les performances de votre programme

Prendre en compte la distance entre le distance entre le mot recherché
et le mot actuel de l'arbre afin de choisir si l'on continue la descente
sur les fils ou non. En se servant de la structure du patricia trie ne
pas recalculer la distance entre les préfixes du mot actuel.

 7.	Que manque-t-il à votre correcteur orthographique pour qu’il soit à l’état de l’art ?

La prise en compte de la position des touches sur un clavier, la
grammaire. Prendre en compte le mappage du clavier
* (Ex: Azerty -> Qwerty).
