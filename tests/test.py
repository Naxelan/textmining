import yaml
import subprocess
import os
import sys
import psutil
import difflib
import time

OK = "\033[32mOK\033[0m"
KO = "\033[31mKO\033[0m"

REFPATH = "./tests/ref/linux/"
DICTNAME = "dict.bin"
REFDICTNAME = "refdict.bin"

class test_stat:
    def __init__(self, title, tpass, total):
        self.title = title
        self.tpass = tpass
        self.total = total

    def __repr__(self):
        return "{}: {}/{} test passed".format(self.title, self.tpass, self.total)

def load_yaml(filename):
    with open(filename, "r", encoding='utf-8') as f:
        return yaml.load(f)

def run(test, binpath, dictpath):
    command = [binpath + "/TextMiningApp", dictpath]
    p = psutil.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    try:
        t1 = time.time()
        out, err = p.communicate(input=test.encode(), timeout=60)
        t = time.time() - t1
    except subprocess.TimeoutExpired:
        p.kill()
        out, err = p.communicate()
        t = 0
    return p, out, err, t

def stdout_diff(out1, out2):
    return '\n'.join(difflib.unified_diff(out1.split("}"), out2.split("}")))

def time_diff(ref, own):
    t1 = ref.cpu_times()
    t2 = ref.cpu_times()
    return t1[0] + t1[1] - (t2[0] + t2[1])

def test_yaml(tyaml):
    title = tyaml["title"]
    tests = tyaml["tests"]
    tpass = 0
    total = 0
    print(title)
    for test in tests:
        total += 1
        process, pout, perr, pt = run(test, "./", DICTNAME)
        ref, rout, rerr, rt = run(test, REFPATH, REFDICTNAME)
        if rout.decode() == pout.decode():
            print("[{}] - {} - time diff: {:04.3f}".format(OK, test, pt- rt))
            tpass += 1
        else:
            print("[{}] - {}".format(KO, test))
            print("stdout diff:\n", stdout_diff(rout.decode(), pout.decode()))
            print("stderr:", perr.decode())
    return test_stat(tyaml["title"], tpass, total)


if __name__ == "__main__":
    stats = []
    files = os.listdir("tests/yaml")
    files.sort()

    for yaml_file in files:
        stat = test_yaml(load_yaml("tests/yaml/" + yaml_file))
        print(stat, "\n")
        stats.append(stat)

    tpass = 0
    total = 0
    for stat in stats:
        print(stat)
        tpass += stat.tpass
        total += stat.total
    print(test_stat("Total", tpass, total))
    if tpass != total:
        sys.exit(1)
