#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "strtab.h"

struct strtab_t strtab_create()
{
        struct strtab_t res = {
                .str = malloc(sysconf(_SC_PAGE_SIZE)),
                .len = 0,
                .capacity = sysconf(_SC_PAGE_SIZE)
        };
        return res;
}

unsigned strtab_insert(struct strtab_t *strtab, char *str)
{
        unsigned res = strtab->len;
        strtab->len += strlen(str) + 1;

        if (strtab->len > strtab->capacity) {
                strtab->str = realloc(strtab->str, strtab->capacity +
                                                   sysconf(_SC_PAGE_SIZE));
                strtab->capacity += sysconf(_SC_PAGE_SIZE);
        }

        strcpy(strtab->str + res, str);

        return res;
}

unsigned strtab_resize(struct strtab_t *strtab)
{
        strtab->str = realloc(strtab->str, strtab->len);
        strtab->capacity = strtab->len;
        return strtab->len;
}

void strtab_delete(struct strtab_t strtab)
{
        free(strtab.str);
}
