#include <err.h>
#include <stdio.h>

#include "dump.h"

void dump_patricia(struct patricia_t *p, FILE *file)
{
        int child = 0;
        int bro = 0;
        if (p->child)
                child = p->child->id;
        if (p->bro)
                bro = p->bro->id;
        struct ftrie_t node = {p->offset, child, bro, p->frequency};
        fwrite(&node, sizeof(struct ftrie_t), 1, file);
        if (child)
                dump_patricia(p->child, file);
        if (bro)
                dump_patricia(p->bro, file);
}

void dump(struct patricia_t *p, char *path, int n, struct strtab_t *strtab)
{
        FILE *file = fopen(path, "w");
        if (file == NULL)
                err(1, "Couldn't open file %s", path);


        unsigned nb_char = strtab_resize(strtab);
        struct header_t header = {n, nb_char};

        printf("node number:%u, char number: %u\n", n, nb_char);

        fwrite(&header, sizeof(struct header_t), 1, file);
        dump_patricia(p, file);
        fwrite(strtab->str, sizeof(char), strtab->len, file);
        fclose(file);
}
