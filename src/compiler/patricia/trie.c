#include <stdlib.h>
#include <string.h>
#include <list.h>
#include <stdio.h>
#include <err.h>

#include "trie.h"
#include "list.h"

struct trie_t *trie_create()
{
        struct trie_t *root = malloc(sizeof(struct trie_t));
        if (!root)
                err(1, "Could not allocate memory");

        root->frequency = 0;
        root->children = list_create();
        return root;
}

int trie_insert(struct trie_t *root, char *word, int frequency)
{
        if (*word == '\0') {
                if (root->frequency)
                        return 0;

                root->frequency = frequency;
                return 1;
        }

        struct list_t *i = root->children->next;
        for (; i != NULL; i = i->next) {
                if (i->element->key == *word)
                        return trie_insert(i->element, word + 1, frequency);
        }
        struct trie_t *new = trie_create();
        new->key = *word;
        list_insert(root->children, new);

        return trie_insert(new, word + 1, frequency);
}

void trie_delete(struct trie_t *root)
{
        list_delete(root->children);

        free(root);
}

void trie_print_rec(struct trie_t *root, char *buff, int len)
{
        if (root->frequency)
                printf("e : %s%c\n", buff, root->key);

        buff[len++] = root->key;
        for (int i = len; i < MAX_WORD_LEN; ++i)
                buff[i] = '\0';

        struct list_t *i = root->children->next;
        for (; i != NULL; i = i->next)
                trie_print_rec(i->element, buff, len);
}

void trie_print(struct trie_t *root)
{
        struct list_t *i = root->children->next;
        for (; i != NULL; i = i->next)
                trie_print_rec(i->element, malloc(MAX_WORD_LEN * sizeof(char)),
                               0);
}
