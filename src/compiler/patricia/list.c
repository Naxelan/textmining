#include <stdlib.h>
#include <err.h>

#include "list.h"
#include "trie.h"

struct list_t *list_create()
{
        struct list_t *head = malloc(sizeof(struct list_t));
        if (!head)
                err(1, "Could not allocate memory");
        head->element = NULL;
        head->next = NULL;
        head->prev = NULL;
        head->len = 0;
        return head;
}

void list_delete(struct list_t *head)
{
        if (head->next)
                list_delete(head->next);
        if (head->element)
                trie_delete(head->element);

        free(head);
}

void list_insert(struct list_t *head, struct trie_t *root)
{
        struct list_t *new = list_create();
        new->next = head->next;

        if (new->next)
                new->next->prev = new;
        head->next = new;
        head->len++;
        new->element = root;
}