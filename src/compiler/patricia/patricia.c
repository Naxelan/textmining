#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <err.h>

#include "patricia.h"
#include "list.h"


void trie_root_delete(struct trie_t *trie)
{
        struct list_t *l = trie->children;
        while (l)
        {
                struct list_t *tmp = l->next;
                free(l);
                l = tmp;
        }
        free(trie);
}

struct patricia_t *patricia_create()
{
        struct patricia_t *root = malloc(sizeof(struct patricia_t));
        if (!root)
                err(1, "Could not allocate memory");

        root->bro = NULL;
        root->offset = 0;
        root->frequency = 0;
        root->child = NULL;

        return root;
}

void
patricia_from_trie_rec(struct patricia_t *node, struct trie_t *trie, int *n,
                       struct strtab_t *strtab);

struct patricia_t *patricia_from_trie_constructor(struct trie_t *trie, int *n,
                                                  struct strtab_t *strtab)
{
        struct patricia_t *res = patricia_create();
        struct list_t *i = trie->children->next;
        patricia_from_trie_rec(res, i->element, n, strtab);

        struct patricia_t *tmp = res;
        for (i = i->next; i != NULL; i = i->next) {
                tmp->bro = patricia_create();
                patricia_from_trie_rec(tmp->bro, i->element, n, strtab);
                tmp = tmp->bro;
        }
        return res;
}

void
patricia_from_trie_rec(struct patricia_t *node, struct trie_t *trie, int *n,
                       struct strtab_t *strtab)
{
        char key[MAX_WORD_LEN];
        int i = 0;
        while (trie->children->len == 1 && !trie->frequency) {
                key[i] = trie->key;
                struct trie_t *tmp = trie;
                trie = trie->children->next->element;
                trie_root_delete(tmp);
                ++i;
        }

        key[i] = trie->key;
        key[i + 1] = '\0';
        node->offset = strtab_insert(strtab, key);
        node->frequency = trie->frequency;
        node->id = *n;
        (*n)++;
        if (trie->children->len != 0)
                node->child = patricia_from_trie_constructor(trie, n, strtab);
}

struct patricia_t *patricia_from_trie(struct trie_t *trie, int *n,
                                      struct strtab_t *strtab)
{
        struct patricia_t *root = patricia_create();
        struct list_t *i = trie->children->next;
        root->child = patricia_create();
        root->id = *n;
        (*n)++;
        patricia_from_trie_rec(root->child, i->element, n, strtab);

        struct patricia_t *tmp = root->child;
        for (i = i->next; i != NULL; i = i->next) {
                tmp->bro = patricia_create();
                patricia_from_trie_rec(tmp->bro, i->element, n, strtab);
                tmp = tmp->bro;
        }
        trie_root_delete(trie);
        return root;
}


void patricia_delete(struct patricia_t *root)
{
        if (!root)
                return;

        patricia_delete(root->bro);
        patricia_delete(root->child);

        free(root);
}


void patricia_print_rec(struct patricia_t *root, char *buff, int len,
                        struct strtab_t strtab)
{
        if (!root)
                return;

        patricia_print_rec(root->bro, buff, len, strtab);

        for (int i = len; i < MAX_WORD_LEN; ++i)
                buff[i] = '\0';

        if (root->frequency)
                printf("%d : %s%s\n", root->id, buff, strtab.str + root->offset);

        strcpy(buff + len, strtab.str + root->offset);
        len += strlen(strtab.str + root->offset);

        patricia_print_rec(root->child, buff, len, strtab);
}

void patricia_print(struct patricia_t *root, struct strtab_t strtab)
{
        patricia_print_rec(root->child, malloc(MAX_WORD_LEN * sizeof(char)),
                           0, strtab);
}
