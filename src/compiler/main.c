#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "dump.h"
#include "parser.h"
#include "patricia.h"
#include "trie.h"

struct trie_t *create_trie_from_file(char *filepath)
{
        struct trie_t *t = trie_create();
        FILE *file = fopen(filepath, "r");
        if (file == NULL)
                err(1, "Could'nt open file %s\n", file);

        struct parsed parsed;
        while (parser(file, &parsed))
                trie_insert(t, parsed.key, parsed.freq);

        fclose(file);
        return t;
}

int main(int argc, char **argv)
{
        if (argc < 3)
                errx(1, "Usage: ./TextMiningCompiler /path/to/word/freq.txt /path/to/dict.bin");
        struct trie_t *t = create_trie_from_file(argv[1]);
        int n = 0;
        struct strtab_t strtab = strtab_create();
        struct patricia_t *p = patricia_from_trie(t, &n, &strtab);

        dump(p, argv[2], n, &strtab);

        patricia_delete(p);
        strtab_delete(strtab);
        return 0;
}
