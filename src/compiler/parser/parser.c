#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "parser.h"

int parser(FILE *file, struct parsed* parsed)
{
        if (fscanf(file, "%s\t%d\n", parsed->key, &(parsed->freq)) !=  EOF){
                return 1;
        }
        return 0;
}
