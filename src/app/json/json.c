#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "json.h"

int word_comparator(const void *t1, const void *t2)
{
        const struct words *word1 = t1;
        const struct words *word2 = t2;
        if (word1->distance != word2->distance)
                return word1->distance - word2->distance;
        if (word2->freq != word1->freq)
                return word2->freq - word1->freq;
        return strcmp(word1->word, word2->word);
}

static inline void print_word(struct words *word)
{
        printf("{\"word\":\"%s\",\"freq\":%u,\"distance\":%u}",
                        word->word,
                        word->freq,
                        word->distance);
}

void json_printer(struct words *words, size_t len)
{
        qsort(words, len, sizeof(struct words), word_comparator);
        printf("[");
        if (len) {
                for (size_t i = 0; i < len - 1; i++) {
                        print_word(words + i);
                        printf(",");
                }
                print_word(words + len - 1);
        }
        printf("]\n");
}
