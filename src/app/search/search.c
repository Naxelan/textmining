#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "search.h"
#include "words.h"
#include "loader.h"
#include "ftrietools.h"
#include "distance.h"

unsigned nb_visited_nodes = 0;

struct words *search_word_rec(char *word, struct bin bin,
                              struct ftrie_t *trie, unsigned offset)
{
        char *str = trie_str(bin, trie);
        if (str[0] != word[offset]) {
                if (trie->id_bro)
                        return search_word_rec(word, bin, trie_bro(bin, trie),
                                               offset);
                return NULL;
        }

        for (unsigned i = 1; str[i] != '\0'; ++i) {
                ++offset;
                if (str[i] != word[offset]) {
                        return NULL;
                }
        }

        if (word[++offset] == '\0') {
                if (trie->freq) {
                        struct words *res = malloc(sizeof(struct words));
                        res->distance = 0;
                        res->freq = trie->freq;
                        res->word = strdup(word);
                        return res;
                }
                return NULL;
        }

        if (trie->id_child)
                return search_word_rec(word, bin, trie_child(bin, trie),
                                       offset);
        return NULL;
}

struct words *search_word(char *word, struct bin bin)
{
        return search_word_rec(word, bin, trie_child(bin, bin.trie), 0);
}

void search_words_rec(char *word, struct bin bin, unsigned approx,
                      struct ftrie_t *trie, unsigned offset, struct search *s,
                      char *buff, unsigned len,
                      unsigned t[MAX_WORD_LEN][MAX_WORD_LEN])
{
        ++nb_visited_nodes;
        if (trie->id_bro)
                search_words_rec(word, bin, approx, trie_bro(bin, trie), offset,
                                 s, buff, len, t);

        for (unsigned i = len; i < MAX_WORD_LEN; ++i)
                buff[i] = '\0';

        char *str = trie_str(bin, trie);
        strcpy(buff + len, str);
        unsigned d = damerau_levenshtein(buff, word, t, len, 0);

        if (trie->freq && d <= approx) {
                if (s->len == s->capacity) {
                        s->words = realloc(s->words, s->capacity * 2 *
                                                     sizeof(struct words));
                        s->capacity *= 2;
                }
                s->words[s->len].freq = trie->freq;
                s->words[s->len].word = strdup(buff);
                s->words[s->len].distance = d;
                ++(s->len);
        }
        if (trie->id_child && t[strlen(buff)][strlen(buff)] <= approx * 2)
                search_words_rec(word, bin, approx, trie_child(bin, trie),
                                 offset, s, buff, len + strlen(str), t);
}

struct search *search_words(char *word, struct bin bin, unsigned approx)
{
        char buff[MAX_WORD_LEN] = {0};
        unsigned t[MAX_WORD_LEN][MAX_WORD_LEN] = {0};
        for (unsigned i = 0; i < MAX_WORD_LEN; ++i) {
                t[0][i] = i;
                t[i][0] = i;
        }
        struct search *res = malloc(sizeof(struct search));
        res->len = 0;
        res->capacity = MAX_WORD_LEN;
        res->words = malloc(sizeof(struct words) * MAX_WORD_LEN);
        search_words_rec(word, bin, approx, trie_child(bin, bin.trie), 0, res,
                         buff, 0, t);
        res->words = realloc(res->words, res->len * sizeof(struct words));
        res->capacity = res->len;
        fprintf(stderr, "Visited %d trie nodes.\n", nb_visited_nodes);
        return res;
}
