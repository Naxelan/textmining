#include "ftrietools.h"

struct ftrie_t *trie_bro(struct bin bin, struct ftrie_t *trie)
{
        return bin.trie + trie->id_bro;
}

struct ftrie_t *trie_child(struct bin bin, struct ftrie_t *trie)
{
        return bin.trie + trie->id_child;
}

char *trie_str(struct bin bin, struct ftrie_t *trie)
{
        return bin.strtab + trie->id_strtab;
}