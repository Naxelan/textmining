#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "loader.h"

int get_file_len(int fd)
{
        struct stat buf;
        if (fstat(fd, &buf) < 0)
                return 0;
        if (buf.st_size < 0)
                return 0;
        return buf.st_size;
}

void load_bin(char *path, struct bin *bin)
{
        int fd = open(path, O_RDONLY);
        if (fd < 0)
                err(1, "Could'nt open file %s", path);

        void *file = mmap(NULL, get_file_len(fd), PROT_READ, MAP_PRIVATE, fd, 0);
        bin->header = file;
        bin->file_len = get_file_len(fd);
        fprintf(stderr, "nb_node: %u, nb_char: %u\n",
                        bin->header->nb_node,
                        bin->header->nb_char);

        bin->trie = (struct ftrie_t*)((char *) file + sizeof(struct header_t));

        unsigned offset = bin->header->nb_node * sizeof(struct ftrie_t)
                                               + sizeof(struct header_t);

        bin->strtab = (char *) file + offset;

        close(fd);
}

void delete_bin(struct bin *bin)
{
        munmap(bin->header, bin->file_len);
}
