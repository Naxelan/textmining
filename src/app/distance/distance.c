#include <string.h>
#include <sys/param.h>
#include <stdio.h>

#include "trie.h"
#include "distance.h"

unsigned damerau_levenshtein(const char *w1, const char *w2,
                             unsigned t[MAX_WORD_LEN][MAX_WORD_LEN],
                             int s1, int s2)
{
        // initialise table
        unsigned l1 = strlen(w1);
        unsigned l2 = strlen(w2);

        //compute distance
        for (unsigned i = s1 + 1; i <= l1; ++i) {
                for (unsigned j = s2 + 1; j <= l2; ++j) {
                        unsigned cost = w1[i - 1] != w2[j - 1];
                        unsigned tmp = MIN(t[i - 1][j] + 1, t[i][j - 1] + 1);
                        t[i][j] = MIN(tmp, t[i - 1][j - 1] + cost);

                        if (i > 1 && w1[i - 2] == w2[j - 1]
                            && j > 1 && w1[i - 1] == w2[j - 2]) {
                                t[i][j] = MIN(t[i][j], t[i - 2][j - 2] + cost);
                        }
                }
        }
        return t[l1][l2];
}
