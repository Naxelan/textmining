#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "loader.h"
#include "json.h"
#include "search.h"

int main(int argc, char **argv)
{
        if (argc < 2)
                errx(1, "Usage ./TextMiningApp /path/to/dict.bin");

        struct bin bin;
        load_bin(argv[1], &bin);

        int approx = 0;
        char word[MAX_WORD_LEN];
        while(scanf("approx %d %s", &approx, word) > 0) {
                fprintf(stderr, "approx %d %s\n", approx, word);
                size_t len = 0;
                struct words *words = NULL;
                if (!approx) {
                        words = search_word(word, bin);
                        if (words)
                                len = 1;
                } else {
                        struct search *s = search_words(word, bin, approx);
                        len = s->len;
                        words = s->words;
                }
                fprintf(stderr, "Found %d words.\n", len);
                json_printer(words, len);
        }
        delete_bin(&bin);

        return 0;
}
