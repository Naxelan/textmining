#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

#include "coroutines.h"

#define COR_SIZE sysconf(_SC_PAGESIZE)
#define PROT_FLAGS (PROT_WRITE | PROT_READ | PROT_EXEC)
#define MAP_FLAGS (MAP_PRIVATE | MAP_ANONYMOUS | MAP_GROWSDOWN | MAP_STACK)

struct coro_t *coro_new()
{
        struct coro_t *coro = calloc(1, sizeof(struct coro_t));
        getcontext(&coro->context);
        coro->context.uc_stack.ss_sp = mmap(NULL,
                                            COR_SIZE,
                                            PROT_FLAGS,
                                            MAP_FLAGS,
                                            -1,
                                            0);
        coro->context.uc_stack.ss_size = COR_SIZE;
        coro->context.uc_stack.ss_flags = 0;
        coro->context.uc_link = NULL;
        return coro;
}

void coro_free(struct coro_t *coro)
{
        munmap(coro->context.uc_stack.ss_sp, COR_SIZE);
        free(coro);
}

void *coro_resume(struct coro_t *coro)
{
        ucontext_t context;
        swapcontext(&coro->old_context, &coro->context);
        return coro->yield_value;
}

void coro_yield(struct coro_t *coro, void *value)
{
        coro->yield_value = value;
        swapcontext(&coro->context, &coro->old_context);
}
